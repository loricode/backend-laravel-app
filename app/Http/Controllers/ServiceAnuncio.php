<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Anuncio;
use App\Models\App;
use Illuminate\Support\Facades\DB;

class ServiceAnuncio extends Controller
{
    public function addAnuncios(Request $request){
        try {
              
             $obj = new Anuncio();
             $titulo =  $request->input('titulo');
             $descripcion =  $request->input('descripcion');
             $imagen =  $request->input('imagen');
             $obj->anuncioTITULO = $titulo;
             $obj->anuncioDESCRIPCION =  $descripcion;
             $obj->anuncioIMAGEN =  $imagen;
             $obj->save();
            //  $data = Anuncio::latest('id')->first();

            //  DB::table('apps_has_anuncios')->insert([
            //   'apps_id' => $id,
            //   'anuncios_id' => $data['id']
            //  ]);
             return json_encode(['msg'=>'anuncios agregado']);
        } catch (Throwable $e) {
            report($e);
    
            return false;
        }
        
    }
}
