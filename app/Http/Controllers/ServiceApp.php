<?php

namespace App\Http\Controllers;
use App\Models\App;
use App\Models\TiposApp;
use Illuminate\Http\Request;

class ServiceApp extends Controller
{
    public function getApps(){
        try {
            $dataApp = App::get();
            $dataTipos = TiposApp::get();
            return json_encode([
              'apps' => $dataApp,
              'tipos' =>$dataTipos 
            ]);
        } catch (Throwable $e) {
            report($e);
    
            return false;
        }
        
    }

    public function addApps(Request $request){
        try {
            $hoy = date("Y-m-d H:i:s");  
            $obj = new App();
            $obj->appTITULO =  $request->input('titulo');
            $obj->appDESCRIPCION =  $request->input('descripcion');
            $obj->appVERSION = $request->input('version');
            $obj->appFECHALANZAMIENTO = $hoy;
            $obj->tipos_apps_id = $request->input('tipo');
            $result = $obj->save();
            return json_encode(['msg'=>'agregado']);

        } catch (Throwable $e) {
            report($e);
    
            return false;
        }   
     }


     public function deleteApp($id){
        try {        
            App::destroy($id);
            return json_encode(["msg"=>"removed"]);
        } catch (Throwable $e) {
            report($e);
            return false;
        }   
    }

    public function getApp($id){
        try {            
            return App::find($id);
        
        } catch (Throwable $e) {
            report($e);
            return false;
        }    
    }


    public function editApp(Request $request, $id){
        try { 
           $titulo =  $request->input('titulo');
           $descripcion =  $request->input('descripcion');
           $version = $request->input('version');
           $tipo= $request->input('tipo');
           App::where('id', $id)->update(
             [
              'appTITULO' => $titulo ,
              'appDESCRIPCION' => $descripcion,
              'appVERSION' => $version,
              'tipos_apps_id' => $tipo
             ]
           );
           return json_encode(["msg"=>"edited"]);

        } catch (Throwable $e) {
            report($e);
            return false;
        }   
     }
}
