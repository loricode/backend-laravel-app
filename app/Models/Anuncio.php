<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anuncio extends Model
{
    public $timestamps = false;
    use HasFactory;


    //relacion muchos a muchos
    public function apps()
    {
        return $this->belongsToMany(App::class);
    }
}
