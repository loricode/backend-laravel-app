<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    public $timestamps = false;
    use HasFactory;


   //relcion uno a muchos

   public function tiposApp()
   {
       return $this->hasMany(TiposApp::class);
   }

    public function anuncios()
    {
        return $this->belongsToMany(Anuncios::class);
    }
}
