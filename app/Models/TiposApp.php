<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiposApp extends Model
{
    public $timestamps = false;
    
    use HasFactory;


   public function oneApp()
   {
       return $this->belongsTo(App::class);
   }
}
