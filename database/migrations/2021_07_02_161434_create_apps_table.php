<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            $table->id();
            $table->string("appTITULO", 45);
            $table->text("appDESCRIPCION");
            $table->string("appVERSION", 45);
            $table->dateTime('appFECHALANZAMIENTO', $precision = 0);
            $table->unsignedBigInteger('tipos_apps_id')->nullable();
            $table->foreign('tipos_apps_id')
                  ->references('id')
                  ->on('tipos_apps')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps');
    }
}
