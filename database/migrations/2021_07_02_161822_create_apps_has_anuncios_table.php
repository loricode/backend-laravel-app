<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppsHasAnunciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps_has_anuncios', function (Blueprint $table) {
            $table->id(); 
                    
            $table->unsignedBigInteger('app_id');
            $table->unsignedBigInteger('anuncio_id');
             //agregado nuevo
            $table->foreign('app_id')->references('id')->on('apps')
            ->onDelete('cascade');
            
            $table->foreign('anuncio_id')->references('id')->on('anuncios')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps_has_anuncios');
    }
}
