<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ServiceApp;
use App\Http\Controllers\ServiceAnuncio;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/apps', [ServiceApp::class, 'getApps']);
Route::get('/apps/{id}', [ServiceApp::class, 'getApp']);
Route::post('/apps', [ServiceApp::class, 'addApps']);

Route::post('/apps/anuncios', [ServiceAnuncio::class, 'addAnuncios']);

Route::delete('/apps/{id}', [ServiceApp::class, 'deleteApp']);
Route::put('/apps/{id}', [ServiceApp::class, 'editApp']);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
